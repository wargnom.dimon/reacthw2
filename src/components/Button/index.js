const Button = ({ bgc, text, onClick = () => {} }) => {
  return (
    <button
      onClick={() => {
        onClick();
      }}
      style={{
        backgroundColor: bgc,
      }}
    >
      {text}
    </button>
  );
};

export default Button;
